class Component {//Definicion de la clase component 
    constructor(structure = {
        selector: '',
        template: '',
        data: {},
        methods: {}
    }) {
        try {/* Asigna los valores a las propiedas correspondientes en base a lo 
        que recibe la clase es la incializacion del objeto cada vez que sea invocado */
            this.selector = `[data-component="${structure.selector}"]`;
            this.container = document.querySelector(this.selector);
            this.template = structure.template;
            this.methods = structure.methods;
            this.data = this.buildData(structure.data);
            this.mount();
        } catch (e) {// Impresion en consola si alguno de los valores que se recive nulo
            console.log(e)
        }
    } 
    buildData(data = {}) {/* Se crea el data y se asignan y obtienen sus propiedades se asigna el data a _data para manipular su instancia y o el objeto
    como tal todo en base al propertyName para hacer la asignacion*/
        var computedInitalData = {}
        this._data = data;
        for (const propertyName in data) { // Se utiliza una constate la cual puede ser asignada en sus propedades pero modificada
            if (data.hasOwnProperty(propertyName)) {
                Object.defineProperty(computedInitalData, propertyName, {
                    set: function (x) {
                        this._data[propertyName] = x;
                        var toChange = this.container.querySelectorAll(`[data-value="${propertyName}"]`);
                        if (toChange) {
                            this.render(toChange, data[propertyName])// Se llma al render enviando la propiedad y el valor que se asignara (funcion abajo definida)
                        }
                    }.bind(this),
                    get: () => (this._data[propertyName]) // Por medio el arrow function se obtiene el valor en la propiedad 
                });
            }
        }
        return computedInitalData; // Regresa el objeto ya definido
    }
    render(element = document.querySelector(this.selector), content = this.template) {
        if (Node.prototype.isPrototypeOf(element)) {
            /* Se aplica el "rendereo" en caso de que el elemnto sea un Input se asigna el ontent 
            que viene del template al elemento*/
            if (element.tagName == "INPUT") {
                element.value = content;
            } else {
                element.innerHTML = content;
            }

        }
        if (NodeList.prototype.isPrototypeOf(element)) {
            element.forEach((child) => {
            /* En caso de que se reciba una lista de elementos lo que se hace es recorerla y volver a llamar a la misma
            funcion para bajar al siguiente nivel y se envian igual la propiedad y su valor*/
                this.render(child, content)
            });
        }
    }
    mount() {
        /* Recorriendo el objeto _data arriba definido se obtienen los data-value que se estan recibiendo y dependiendo de 
        si es un elemento tipo input o checkbox se agregan los listener; si es un input se asigna el value y si es un checkbox se asigna el reultado de la
        del click, para el input se escucha el "input" y para el checkbox el "checked"*/
        this.render(this.container, this.template);
        for (const key in this._data) {
            if (this.data.hasOwnProperty(key)) {
                var toChange = this.container.querySelectorAll(`[data-value="${key}"]`);
                if (toChange) {
                    this.render(toChange, this.data[key]);
                    toChange.forEach(element => {
                        if (element.tagName == 'INPUT') {
                                element.addEventListener('input', event=> {
                                this.data[key] = event.target.value;
                                })
                            if (element.getAttribute('type') == 'checkbox') {
                                    element.addEventListener('click', (click) => {
                                        this.data[key] = click.target.checked
                                        })
                                    }
                        }
                    });
                }
            }
        }
        const events = ['click'] // se asignan las propiedades event (el vent que se aplica y y del container la lista de data-event pasadas)
        const DOMElements = events.map(event => {
            return {
                event: event,
                targets: this.container.querySelectorAll(`[data-event-${event}]`)
            };
        });
        for (const method in this.methods) { //Se recorren las propiedas en el objeto mehtod y se asignan en base al objeto que se tiene
            if (this.methods.hasOwnProperty(method)) {
                this.methods[method] = this.methods[method].bind(this);
            }
        }
        DOMElements.forEach(element => { /* Se agrega un listener al elemento recorriendo los targets y asi se asigna a cada
        target un listener del method */
            element.targets.forEach(target => {
                target.addEventListener(element.event, this.methods[target.getAttribute('data-event-' + element.event)]);
            });
        });
    }
}