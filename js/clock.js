class Clock extends Component {
    constructor() { //Constructor del objeto component
        super({
            selector: "clock", // Steo de la propiedad selector, en este caso asigna clock
            template: `// definicion del template en HTML
                    <article class="reloj">
                        <section class="display">
                            <span class="hours" data-value="hours">0</span>
                            <span class="minutes" data-value="minutes">0</span>
                            <span class="seconds" data-value="seconds">0</span>
                        </section>
                        <section class="controls">
                            <button data-event-click="startClock">Start</button>
                            <button data-event-click="pauseClock">Pause</button>
                            <button data-event-click="stopClock">Stop</button>
                        </section>
                    </article>`,
            data: { // inicilizacion de la data en 0 de las propiedades para iniciar el contador
                seconds: 0,
                minutes: 0,
                hours: 0
            },
            methods: {//Metodo para manejar los intervalos de tiempo delos min, seg y hr
                updateClock() { // Metodo para actualizar el reloj
                    if (this.data.seconds < 59) { //Incrementa el segundero hasta antes de llegar al minuto
                        this.data.seconds++;
                    } else { // si se llego a los 59 segundos reinicia el segundero e incrementa el minutero
                        this.data.seconds = 0;
                        this.data.minutes++;
                    }
                    if (this.data.minutes >= 60) {// reinicia los mintos al llegar a 60 (porque se cumple la hora) e increment la hoa en 1
                        this.data.minutes = 0;
                        this.data.hours++;
                    }
                },
                startClock() {// Se incia el conteo
                    if (!this.data.clockId) {
                        this.data.clockId = setInterval(function () {// cuncion que manda a llamra la definida arriba para hacer el conteo
                            this.methods.updateClock();
                        }.bind(this), 100);// NO SE PORQUE EL 100  ¿?
                    }

                },
                pauseClock() {//  Detiene el reloj y lo setea a 0 para que no continue con el conteo
                    clearInterval(this.data.clockId)
                    this.data.clockId = 0;
                },
                stopClock() {// Funcion que detiene el conteo y reinicia todas las porpiedases (seg, min, hr y id) para comenzar de 0
                    clearInterval(this.data.clockId)
                    this.data.seconds = 0;
                    this.data.minutes = 0;
                    this.data.hours = 0;
                    this.data.clockId = 0;
                }
            }
        });
    }
};